# Authority or hub

import re
import sys

name = sys.argv[-1]

if (name == 'ekantipur'):
    pattern = 'ekantipur.com'
    filepath = 'extracted/ekantipur_links.txt'
elif (name == 'nagariknews'):
    pattern = 'nagariknews.com'
    filepath = 'extracted/nagariknews_links.txt'
elif (name == 'onlinekhabar'):
    pattern = 'onlinekhabar.com'
    filepath = 'extracted/onlinekhabar_links.txt'
elif (name == 'pahilopost'):
    pattern = 'pahilopost.com'
    filepath = 'extracted/pahilopost_links.txt'
elif (name == 'setopati'):
    pattern = 'setopati.com'
    filepath = 'extracted/setopati_links.txt'
else:
    print('Invalid argument')
    sys.exit(1)

with open(filepath) as fp:
    lines = fp.read().split('\n')

# Unique list from
unique_links = list(set(lines))

internal_links = 0;
external_links = 0;

for link in unique_links:
    # Naive regex check for internal links
    match = re.findall(pattern, link)
    if (match):
        internal_links = internal_links + 1
    else:
        external_links = external_links + 1

print('-' * len(name))
print(name)
print('-' * len(name))
print('number_of_links:', len(lines))
print('unique_links:', len(unique_links))
print('internal_links:', internal_links)
print('external_links:', external_links)
print('-' * len(name))

if (internal_links > external_links):
    print(name + ' is an authority.')
else:
    print(name + ' is a hub.')

print('-' * len(name))
