import scrapy

class NagarikSportsContentSpider(scrapy.Spider):
    name = 'nagariknews_sports_content'

    with open('extracted/nagariknews_sports_links.txt') as fp:
        lines = fp.read().split('\n')

    unique_links = list(set(filter(None, lines)))

    start_urls = unique_links

    def parse(self, response):
        # content
        p = response.css('div.news-content p::text').extract()
        text = ' '.join(p)

        with open('extracted/sports_content.csv', 'a') as f:
            f.write(text + '\n')
