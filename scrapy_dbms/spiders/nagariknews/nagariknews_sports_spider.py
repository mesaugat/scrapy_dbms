import scrapy

class NagariknewsSportsSpider(scrapy.Spider):
    name = 'nagariknews_sports'

    start_urls = [
        # nagarik news
        'http://nagariknews.com/category/26',
        'http://nagariknews.com/category/26?page=2',
        'http://nagariknews.com/category/26?page=3',
        'http://nagariknews.com/category/26?page=4',
        'http://nagariknews.com/category/26?page=5'
    ]

    def parse(self, response):
        filename = 'extracted/' + self.name + '_links.txt'

        # content
        links = response.css('div.first-list a::attr(href)').extract()

        with open(filename, 'a') as f:
            for link in links:
                self.log(link)
                f.write('http://nagariknews.com' + link + '\n')
