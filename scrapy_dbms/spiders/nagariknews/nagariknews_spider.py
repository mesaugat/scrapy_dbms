import scrapy
from scrapy.linkextractors import LinkExtractor

class NagariknewsSpider(scrapy.Spider):
    name = 'nagariknews'

    def start_requests(self):
        urls = [
            'http://nagariknews.com',

            # politics
            'http://www.nagariknews.com/category/21',

            # social
            'http://www.nagariknews.com/category/24',

            # art and entertainment
            'http://www.nagariknews.com/category/25'

            # sports
            'http://www.nagariknews.com/category/26',

            # world
            'http://www.nagariknews.com/category/27',

            # technology
            'http://www.nagariknews.com/category/33',

            # opinion
            'http://www.nagariknews.com/category/81',

            # interview
            'http://www.nagariknews.com/category/82'
        ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        extractor = LinkExtractor()
        links = extractor.extract_links(response)
        filename = 'extracted/' + self.name + '_links.txt'

        with open(filename, 'a') as f:
            for link in links:
                self.log(link)
                f.write(link.url + '\n')
