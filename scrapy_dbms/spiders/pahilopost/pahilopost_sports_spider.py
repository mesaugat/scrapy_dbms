import scrapy

class PahilopostSportsSpider(scrapy.Spider):
    name = 'pahilopost_sports'

    start_urls = [
        # pahilo post
        'http://www.pahilopost.com/content/category/sports.html'
    ]

    def parse(self, response):
        filename = 'extracted/' + self.name + '_links.txt'

        # primary content
        div = response.css('div.uk-width-medium-7-10')[0]
        primary_links = div.css('a::attr(href)').extract()

        # secondary content
        ul = response.css('ul.pahilo-secondary-article')[0]
        secondary_links = div.css('a::attr(href)').extract()

        links = primary_links + secondary_links
        with open(filename, 'a') as f:
            for link in links:
                self.log(link)
                f.write(link + '\n')
