import scrapy
from scrapy.linkextractors import LinkExtractor

class PahilopostSpider(scrapy.Spider):
    name = 'pahilopost'

    def start_requests(self):
        urls = [
            'http://pahilopost.com',
            'http://www.pahilopost.com/content/category/crime.html',
            'http://www.pahilopost.com/content/category/sports.html',
            'http://www.pahilopost.com/content/category/politics.html',
            'http://www.pahilopost.com/content/category/national.html',
            'http://www.pahilopost.com/content/category/lifestyle.html',
            'http://www.pahilopost.com/content/category/technology.html',
            'http://www.pahilopost.com/content/category/photo-news.html',
            'http://www.pahilopost.com/content/category/literature.html',
            'http://www.pahilopost.com/content/category/entertainment.html'
        ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        extractor = LinkExtractor()
        links = extractor.extract_links(response)
        filename = 'extracted/' + self.name + '_links.txt'

        with open(filename, 'a') as f:
            for link in links:
                self.log(link)
                f.write(link.url + '\n')
