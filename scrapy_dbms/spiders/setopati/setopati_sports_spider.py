import scrapy

class SetopatiSportsSpider(scrapy.Spider):
    name = 'setopati_sports'

    start_urls = [
        # setopati
        'http://setopati.com/sports',
        'https://setopati.com/sports/page/2',
        'https://setopati.com/sports/page/3',
        'https://setopati.com/sports/page/4',
        'https://setopati.com/sports/page/5',
    ]

    def parse(self, response):
        filename = 'extracted/' + self.name + '_links.txt'

        # content
        links = response.css('div.post-thumbnail a::attr(href)').extract()

        with open(filename, 'a') as f:
            for link in links:
                self.log(link)
                f.write(link + '\n')
