import scrapy

class SetopatiSportsContentSpider(scrapy.Spider):
    name = 'setopati_sports_content'

    with open('extracted/setopati_sports_links.txt') as fp:
        lines = fp.read().split('\n')

    unique_links = list(set(filter(None, lines)))

    start_urls = unique_links

    def parse(self, response):
        # content
        p = response.css('div.entry-content p::text').extract()
        p = list(map(str.strip, p))
        text = ' '.join(p)

        self.log(text)

        with open('extracted/sports_content.txt', 'a') as f:
            f.write(text + '\n')
