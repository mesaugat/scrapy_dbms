import scrapy
from scrapy.linkextractors import LinkExtractor

class SetopatiSpider(scrapy.Spider):
    name = 'setopati'

    def start_requests(self):
        urls = [
            'http://setopati.com',

            # politics
            'https://setopati.com/politics',
            'https://setopati.com/politics/page/2',
            'https://setopati.com/politics/page/3',
            'https://setopati.com/politics/page/4',
            'https://setopati.com/politics/page/5',

            # social
            'https://setopati.com/social',
            'https://setopati.com/social/page/2',
            'https://setopati.com/social/page/3',
            'https://setopati.com/social/page/4',
            'https://setopati.com/social/page/5',

            # global
            'https://setopati.com/global',
            'https://setopati.com/global/page/2',
            'https://setopati.com/global/page/3',
            'https://setopati.com/global/page/4',
            'https://setopati.com/global/page/5',

            # sports
            'http://setopati.com/sports',
            'https://setopati.com/sports/page/2',
            'https://setopati.com/sports/page/3',
            'https://setopati.com/sports/page/4',
            'https://setopati.com/sports/page/5',

            # opinion
            'https://setopati.com/opinion',
            'https://setopati.com/opinion/page/2',
            'https://setopati.com/opinion/page/3',
            'https://setopati.com/opinion/page/4',
            'https://setopati.com/opinion/page/5'
        ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        extractor = LinkExtractor()
        links = extractor.extract_links(response)
        filename = 'extracted/' + self.name + '_links.txt'

        with open(filename, 'a') as f:
            for link in links:
                self.log(link)
                f.write(link.url + '\n')
