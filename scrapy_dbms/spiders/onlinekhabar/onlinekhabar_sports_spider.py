import scrapy

class OnlinekhabarSportsSpider(scrapy.Spider):
    name = 'onlinekhabar_sports'

    start_urls = [
        # online khabar
        'http://www.onlinekhabar.com/content/sports/',
        'http://www.onlinekhabar.com/content/sports/page/2',
        'http://www.onlinekhabar.com/content/sports/page/3',
        'http://www.onlinekhabar.com/content/sports/page/4',
        'http://www.onlinekhabar.com/content/sports/page/5'
    ]

    def parse(self, response):
        filename = 'extracted/' + self.name + '_links.txt'

        # content
        divs = response.css('div.news_loop')
        links = []

        for div in divs:
            link = div.css('a::attr(href)').extract_first();
            links.append(link)

        # write links to a file

        with open(filename, 'a') as f:
            for link in links:
                self.log(link)
                f.write(link + '\n')
