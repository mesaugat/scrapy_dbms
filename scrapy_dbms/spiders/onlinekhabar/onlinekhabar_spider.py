import scrapy
from scrapy.linkextractors import LinkExtractor

class OnlineKhabarSpider(scrapy.Spider):
    name = 'onlinekhabar'

    def start_requests(self):
        urls = [
            'http://onlinekhabar.com',

            # news
            'http://www.onlinekhabar.com/content/news/'
            'http://www.onlinekhabar.com/content/news/page/2',
            'http://www.onlinekhabar.com/content/news/page/3',
            'http://www.onlinekhabar.com/content/news/page/4',
            'http://www.onlinekhabar.com/content/news/page/5',

            # opinion
            'http://www.onlinekhabar.com/content/opinion/',
            'http://www.onlinekhabar.com/content/opinion/page/2',
            'http://www.onlinekhabar.com/content/opinion/page/3',
            'http://www.onlinekhabar.com/content/opinion/page/4',
            'http://www.onlinekhabar.com/content/opinion/page/5',

            # entertainment
            'http://www.onlinekhabar.com/content/entertainment/',
            'http://www.onlinekhabar.com/content/entertainment/page/2',
            'http://www.onlinekhabar.com/content/entertainment/page/3',
            'http://www.onlinekhabar.com/content/entertainment/page/4',
            'http://www.onlinekhabar.com/content/entertainment/page/5',

            # business
            'http://www.onlinekhabar.com/content/business/',
            'http://www.onlinekhabar.com/content/business/page/2',
            'http://www.onlinekhabar.com/content/business/page/3',
            'http://www.onlinekhabar.com/content/business/page/4',
            'http://www.onlinekhabar.com/content/business/page/5',

            # sports
            'http://www.onlinekhabar.com/content/sports/',
            'http://www.onlinekhabar.com/content/sports/page/2',
            'http://www.onlinekhabar.com/content/sports/page/3',
            'http://www.onlinekhabar.com/content/sports/page/4',
            'http://www.onlinekhabar.com/content/sport/pages/5'
        ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        extractor = LinkExtractor()
        links = extractor.extract_links(response)
        filename = 'extracted/' + self.name + '_links.txt'

        with open(filename, 'a') as f:
            for link in links:
                self.log(link)
                f.write(link.url + '\n')
