import scrapy

class OnlinekhabarSportsContentSpider(scrapy.Spider):
    name = 'onlinekhabar_sports_content'

    with open('extracted/onlinekhabar_sports_links.txt') as fp:
        lines = fp.read().split('\n')

    unique_links = list(set(filter(None, lines)))

    start_urls = unique_links

    def parse(self, response):
        # content
        p = response.css('div.ok-single-content p::text').extract()
        text = ' '.join(p)

        self.log(text)

        with open('extracted/sports_content.txt', 'a') as f:
            f.write(text + '\n')
