import scrapy
from scrapy.linkextractors import LinkExtractor

class EkantipurSpider(scrapy.Spider):
    name = 'ekantipur'

    def start_requests(self):
        urls = [
            'https://ekantipur.com/nep/world',
            'https://ekantipur.com/nep/nepal',
            'https://ekantipur.com/nep/sports',
            'https://ekantipur.com/nep/others',
            'https://ekantipur.com/nep/economy',
            'https://ekantipur.com/nep/politics',
            'https://ekantipur.com/nep/entertainment'
        ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        extractor = LinkExtractor()
        links = extractor.extract_links(response)
        filename = 'extracted/' + self.name + '_links.txt'

        with open(filename, 'a') as f:
            for link in links:
                self.log(link)
                f.write(link.url + '\n')
