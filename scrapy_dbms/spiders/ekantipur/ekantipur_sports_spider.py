import scrapy

class EkantipurSportsSpider(scrapy.Spider):
    name = 'ekantipur_sports'

    start_urls = [
        # ekantipur
        'http://www.ekantipur.com/nep/sports'
    ]

    def parse(self, response):
        filename = 'extracted/' + self.name + '_links.txt'

        # content
        divs = response.css('div.news-title-cat')
        links = []

        for div in divs:
            link = div.css('a::attr(href)').extract_first()
            links.append(link)

        with open(filename, 'a') as f:
            for link in links:
                self.log(link)
                f.write(link + '\n')
