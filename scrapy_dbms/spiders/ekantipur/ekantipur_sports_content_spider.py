import scrapy

class EkantipurSportsContentSpider(scrapy.Spider):
    name = 'ekantipur_sports_content'

    with open('extracted/ekantipur_sports_links.txt') as fp:
        lines = fp.read().split('\n')

    unique_links = list(set(filter(None, lines)))

    start_urls = unique_links

    def parse(self, response):
        # content
        p = response.css('div.content-wrapper p::text').extract()
        text = ' '.join(p)

        self.log(text)

        with open('extracted/sports_content.txt', 'a') as f:
            f.write(text + '\n')
