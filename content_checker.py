# Get most commonly used words from sports content

import re
from collections import Counter

filepath = 'extracted/sports_content.txt'
stopwords = 'extracted/stopwords.txt'

file = open(filepath, 'r')
content = ' '.join(file.read().split())
file.close()

for ch in ['।', ',', '\'', '"']:
    if ch in content:
        content = content.replace(ch, '')

content_list = content.split()

with open('extracted/stopwords.txt') as fp:
    lines = fp.read().split('\n')

stopwords = list(set(filter(None, lines)))

resultwords = [word for word in content_list if word not in stopwords]

stats = Counter(resultwords)
most_common_list = stats.most_common(40)

with open('extracted/most_common.txt', 'w') as f:
    for k,v in most_common_list:
        f.write('{} {}\n'.format(k,v))
